import os

from dotenv import load_dotenv
from flask.globals import request
from flask.helpers import flash, url_for

dotenv_path = os.path.join(os.path.dirname(__file__), ".env")
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

from flask import render_template
from jinja2.exceptions import TemplateNotFound

from app import create_app

app = create_app(os.getenv("FLASK_CONFIG") or "default")

# Errors
@app.errorhandler(403)
def access_forbidden(error):
    return render_template("error-403.html", err_code=403), 403


@app.errorhandler(404)
@app.errorhandler(TemplateNotFound)
def not_found_error(error):
    return render_template("error-404.html", err_code=404), 404


@app.errorhandler(500)
def internal_error(error):
    return render_template("error-500.html", err_code=500), 500


@app.errorhandler(401)
def unauthorized_error(error):
    next = request.args.get("next")
    flash("ابتدا وارد حساب کاربری خود شوید", category="danger")

    return url_for("auth_bp", next=next)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5001)
