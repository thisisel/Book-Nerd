import logging
import os
from datetime import timedelta

import redis

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    FLASK_ENV = os.environ.get("FLASK_ENV")

    REDIS_URL = os.environ.get("REDIS_URL", "redis://127.0.0.1:6379")

    SECRET_KEY = os.environ.get("SECRET_KEY", os.urandom(24))

    SESSION_PERMANENT = True
    PERMANENT_SESSION_LIFETIME = timedelta(hours=2)
    SESSION_COOKIE_HTTPONLY = True
    SESSION_TYPE = "redis"
    SESSION_REDIS = redis.from_url(REDIS_URL)
    SESSION_USE_SIGNER = True
    SESSION_PERMANENT = False

    DEBUG = False

    ASSETS_DEBUG = False
    ASSETS_AUTO_BUILD = True


class DevelopmentConfig(Config):
    DEBUG = True

    # Add logger
    logging.basicConfig(
        format="%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s",
        datefmt="%Y-%m-%d:%H:%M:%S",
        filename="client-dev.log",
        level=logging.DEBUG,
    )


class TestingConfig(Config):
    # [Testing server address]:[port]
    SERVER_BASE_URL = os.environ.get("TESTING_SERVER_URL")

    DEBUG = True
    TESTING = True


class ProductionConfig(Config):
    # [Production server address]:[port]
    SERVER_BASE_URL = os.environ.get("PRODUCTION_SERVER_URL")
    REDIS_URL = os.environ.get("REDIS_URL", None)

    DEBUG = False


config_by_name = dict(
    development=DevelopmentConfig,
    testing=TestingConfig,
    production=ProductionConfig,
    default=DevelopmentConfig,
)
