# Container-less approach

## 1. Clone repository

```bash
$ git clone git@github.com:thisisel/flask_client.git
$ cd flask_client
```

## 2 Install and activate virtual environment

```
poetry shell
poetry install
```

## 6. Run

```bash
$ flask run -h 0.0.0.0 -p 5001

#alternative
$ poetry run wsgi.py

```

# Docker

## Build

```
docker build -t nerd-flask-client .
```

## Run

```
docker run --name flaskclient -it -v $PWD/app:/client/app -p 5001:5001 nerd-flask-client
```
