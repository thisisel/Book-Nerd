import requests
from flask import session
from requests.sessions import Session
from werkzeug.wrappers import response

from app.schemas.user import BaseUserSchema
from app.utils.http_components import FastApiHeaders, FastApiUrls

from .forms import EdithUserInfoForm


class ProfileService:
    @staticmethod
    def edit_profile(form: EdithUserInfoForm):
        user_update: BaseUserSchema = BaseUserSchema(
            email=form.email.data,
            shahr_id=form.city.data,
            ostan_id=form.state.data,
            phonenumber=form.phonenumber.data,
            username=form.username.data,
            firstname=form.username.data,
            lastname=form.lastname.data,
        )

        header = FastApiHeaders.MULTIPART_FORM
        header.update({"Authorization": "Bearer " + session["access_token"]})

        data_response = requests.patch(
            url=FastApiUrls.PROFILE_ME,
            data=user_update.dict(exclude_unset=True, exclude_none=True),
            headers=header,
        )

        if data_response.ok:

            ...

        if profile_img := form.img.data:
            multipart_form_file = {
                "profile_img": profile_img,
            }
            img_response = requests.patch(
                url=FastApiUrls.PROFILE_ME,
                files=multipart_form_file,
                headers=header,
            )

    ...
