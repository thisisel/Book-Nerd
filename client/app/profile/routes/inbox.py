from . import profile_bp


@profile_bp.route("/inbox")
def inbox_overview():
    pass


@profile_bp.route("/inbox/<int:id>")
def inbox_request(id):
    pass


@profile_bp.route("/inbox/<int:id>/update-status", methods=["GET", "POST"])
def update_inbox_request(id):
    pass
