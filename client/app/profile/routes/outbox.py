from . import profile_bp


@profile_bp.route("/outbox")
def outbox_overview():
    pass


@profile_bp.route("/outbox/<int:id>")
def outbox_request(id):
    pass


@profile_bp.route("/outbox/<int:id>/cancel", methods=["GET", "POST"])
def cancel_outbox_request(id):
    pass
