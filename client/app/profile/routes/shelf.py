from flask import flash, redirect, render_template, request, session, url_for

from app.utils.http_components.urls import FastApiUrls

from ..forms import ShelfItemForm
from . import profile_bp


@profile_bp.route("/shelf/<int:id>/edit")
def edit_item(id):

    form = ShelfItemForm()
    return render_template("add-item.html", form=form, title="ویرایش کتاب")


@profile_bp.route("/shelf/<int:id>/archive")
def archive_item(id):

    form = ShelfItemForm()
    return render_template(
        "personal-profile.html", form=form, title="ویرایش کتاب",
    )


@profile_bp.route("/shelf/add-item", methods=["GET", "POST"])
def search_repo():
    from ..forms import BasicSearchBook

    form = BasicSearchBook()

    if form.validate_on_submit():
        isbn = form.isbn.data

        url = FastApiUrls.EXPLORE_BOOK + isbn
        resp = request.get(url)

        if resp.ok:
            r_json = resp.json()
            book = r_json["book"]

            return redirect(url_for("profile_bp.add_item", book=book))

    return render_template(
        "basic-search.html",
        form=form,
        title="افزودن کتاب",
        legend="۱. جست‌وجوی کتاب",
    )


@profile_bp.route("/shelf/add-item/book", methods=["GET", "POST"])
def add_item(book):
    pass


@profile_bp.route("/shelf/<int:id>/delete")
def delete_item():
    pass
