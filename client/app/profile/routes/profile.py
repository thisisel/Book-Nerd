from flask import render_template, request

from app.utils.decorators import login_required
from app.utils.security import current_user

from ..forms import ChangePasswordForm, EdithUserInfoForm
from . import profile_bp


@profile_bp.route("/", methods=["GET", "POST"])
# @login_required
def profile():
    # TODO autofil via cache rather than request
    edit_info_form = EdithUserInfoForm()
    change_pass_form = ChangePasswordForm()

    if edit_info_form.validate_on_submit():
        ...
    if change_pass_form.validate_on_submit():
        ...
    if request.method == "GET":

        return render_template(
            "personal-profile.html",
            title="پروفایل",
            form=edit_info_form,
            change_pass_form=change_pass_form,
            current_user=current_user,
        )


@profile_bp.route("/shelf/<int:id>")
def item_detail(id):
    # TODO send request to GET /api/users/{username}/shelf/{id}

    return render_template("book_detail.html")


@profile_bp.route("/request")
def request():

    return render_template("LendStatus.html", title="درخواست ها")
