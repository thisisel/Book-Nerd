from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileField
from wtforms import (
    BooleanField,
    IntegerField,
    PasswordField,
    SelectField,
    StringField,
    SubmitField,
    TextAreaField,
)
from wtforms.fields.html5 import DateField
from wtforms.validators import (
    DataRequired,
    Email,
    EqualTo,
    Length,
    ValidationError,
    email_validator,
)


class ShelfItemForm(FlaskForm):
    title = StringField("نام کتاب", validators=[DataRequired()])
    isbn = StringField("شابک", validators=[DataRequired()])
    registered_date = DateField("تاریخ ثبت", validators=[DataRequired()])
    quality = SelectField(
        "کیفیت کتاب",
        choices=[
            ("new", "New"),
            ("second handed - excellent", "Second Handed - Excellent"),
            ("second handed - good", "Second Handed - Good"),
            ("second handed - poor", "Second Handed - Poor"),
        ],
        validators=[DataRequired()],
    )
    img = FileField("بارگزاری  تصویر", validators=[FileAllowed(["jpg", "png"])])
    is_availabl = BooleanField(label="در دسترس")
    in_public_view = BooleanField(label="نمایش در پروفایل")
    borrow_interval = IntegerField(
        "روزهای مجاز امانت", validators=[DataRequired()],
    )
    submit = SubmitField("ثبت")


class BasicSearchBook(FlaskForm):
    isbn = StringField(
        "ISBN",
        validators=[DataRequired(), Length(min=10, max=13)],
        id="choices-text-preset-values",
    )
    submit = SubmitField("Search")


class EdithUserInfoForm(FlaskForm):

    username = StringField("نام کاربری", validators=[Length(min=2, max=20)])
    img = FileField("بارگزاری  تصویر", validators=[FileAllowed(["jpg", "png"])])
    email = StringField("ایمیل", validators=[Email()])
    phonenumber = StringField("تلفن همراه", validators=[Length(min=12, max=12)])
    firstname = StringField("نام ")
    lastname = StringField(" نام خانوادگی ")
    city = SelectField("شهر")
    state = SelectField(
        " استان ",
        choices=[
            ("1", "آذربایجان شرقی"),
            ("2", "آذربایجان غربی"),
            ("3", "اردبیل"),
            ("4", "اصفهان"),
        ],
    )
    submit = SubmitField("ثبت")


class ChangePasswordForm(FlaskForm):
    password = PasswordField("  گذرواژه قبل", validators=[DataRequired()])
    new_Password = PasswordField(" گذرواژه جدید", validators=[DataRequired()])
    confirm_password = PasswordField(
        "تکرار گذرواژه", validators=[DataRequired(), EqualTo("new_Password")],
    )

    submit = SubmitField("تغییر")


class DeletAccuntForm(FlaskForm):
    password = PasswordField("گذرواژه", validators=[DataRequired()])
    submit = SubmitField("ثبت")
