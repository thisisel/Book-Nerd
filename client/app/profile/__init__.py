from flask import Blueprint

profile_bp = Blueprint(
    "profile_bp", __name__, static_folder="static", template_folder="templates",
)
