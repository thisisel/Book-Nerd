import json
from typing import Union

import requests
from flask import (
    abort,
    current_app,
    flash,
    redirect,
    render_template,
    request,
    session,
    url_for,
)

from . import home_bp


@home_bp.route("/")
def index():
    # TODO request books
    from app.utils.mock_server import books

    # TODO decorator
    if (current_user := session.get("email")) is None:
        current_user = 0

    return render_template(
        "index.html",
        title="صفحه‌ی اصلی",
        books=books,
        current_user=current_user,
    )


@home_bp.route("/idea")
def idea():
    return render_template("idea.html")


@home_bp.route("/contact-us")
def contact():
    return render_template("connect.html")


# test route for consuming API
@home_bp.route("/ping")
def ping():

    from app.utils.http_components import FastApiUrls

    r = requests.get(url=FastApiUrls.API_ROOT)

    return r.json()
