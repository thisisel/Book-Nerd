from flask import Blueprint

users_bp = Blueprint(
    "users_bp", __name__, static_folder="static", template_folder="templates",
)
