# from app.profile.routes.profile import profile
from flask import render_template

from . import users_bp


@users_bp.route("/<string:username>/<int:id>")
def view_item(username, id):
    from .services import PublicShelfItemService

    item = PublicShelfItemService.get_item(id)
    return render_template(
        "item.html", title=f"پروفایل {username}", username=username, item=item,
    )


# @users_bp.route("/<string:username>")
# def view_shelf(username):
#     return render_template("")


@users_bp.route("/<string:username>")
def user_profile(username):
    from .services import PublicShelfService

    items = PublicShelfService.get_shelf(username=username)

    return render_template(
        "public-profile.html",
        title=f"پروفایل {username}",
        items=items[0:3],
        username=username,
    )
