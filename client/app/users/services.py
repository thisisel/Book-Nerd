from app.utils.mock_server import books


class PublicShelfService:
    @staticmethod
    def get_shelf(username: str):
        # TODO send request
        return books


class PublicShelfItemService:
    @staticmethod
    def get_item(id: int):
        # TODO send request
        return books[id]
