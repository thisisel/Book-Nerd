from os import stat
from typing import Any, Optional
from datetime import datetime

from pydantic import BaseModel
from pydantic.networks import EmailStr


class Sessionschema(BaseModel):
    access_token: str
    user_email: EmailStr
    username: Optional[str]
    joined_date: Optional[datetime]


class InternalMessage(BaseModel):
    status: bool
    code: Optional[int]
    message: str
    data: Optional[Any]