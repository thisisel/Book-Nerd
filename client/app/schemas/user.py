from typing import Optional
from pydantic import BaseModel, validator
from pydantic.networks import EmailStr
from flask import session

class BaseUserSchema(BaseModel):
    email: Optional[EmailStr]
    access_token: Optional[str]
    shahr_id: Optional[str]
    ostan_id: Optional[str]
    phonenumber: Optional[str]
    username: Optional[str]
    firstname: Optional[str]
    lastname: Optional[str]
    lastname: Optional[str]
    profile_img_path: Optional[str]


class CurrentUserSchema(BaseUserSchema):
    email: EmailStr
    access_token: str

    @validator("email", pre=True, always=True)
    def set_email(cls, v):
        session["email"] = v
        return v 

    @validator("access_token", pre=True, always=True)
    def set_token(cls, v):
        session["access_token"] = v
        return v 

