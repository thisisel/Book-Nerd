from typing import List
from pydantic import BaseModel
from pydantic.networks import EmailStr


class LoginPayloadschema(BaseModel):
    username: EmailStr
    password: str


class LoginAPISuccessResponse(BaseModel):
    access_token: str
    token_type: str


class LoginAPIFailedResponse(BaseModel):
    errors: List[str]