""" Top level module

This module:

- Contains create_app()
- Registers extensions
- Registers bluprints
"""

from flask import Flask

from app.utils.asset_compiler import compile_static_assets
from app.utils.extensions import assets, cors, csrf, ma, ses
from config import config_by_name


def create_app(config_name):

    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])

    with app.app_context():

        register_extensions(app)
        register_blueprints(app)

        compile_static_assets(assets)
        # TODO check
        from app import utils

    return app


def register_extensions(app):
    # Registers flask extensions

    assets.init_app(app)
    ma.init_app(app)
    cors.init_app(app)
    csrf.init_app(app)
    ses.init_app(app)


def register_blueprints(app):
    # Register blueprints and routes

    from .auth import routes as auth_routes

    app.register_blueprint(auth_routes.auth_bp, url_prefix="/auth")

    from .home import routes as home_routes

    app.register_blueprint(home_routes.home_bp, url_prefix="/")

    from .profile import routes as profile_routes

    app.register_blueprint(profile_routes.profile_bp, url_prefix="/profile")

    from .users import routes as users_routes

    app.register_blueprint(users_routes.users_bp, url_prefix="/users")

    from .explore import routes as explore_routes

    app.register_blueprint(explore_routes.explore_bp, url_prefix="/explore")
