import random

from flask import url_for

books = [
    {
        "id": 1,
        "isbn": "",
        "title": "Gone with the wind",
        "author": "",
        "translated_by": "",
        "img": url_for("static", filename="img/gone-wind.jpg"),
    },
    {
        "id": 2,
        "isbn": "",
        "title": "The Alchemist",
        "author": "",
        "translated_by": "",
        "img": url_for("static", filename="img/alchemist.jpg"),
    },
    {
        "id": 3,
        "isbn": "",
        "title": "Rebeka",
        "author": "",
        "translated_by": "",
        "img": url_for("static", filename="img/REBEKA.jpg"),
    },
    {
        "id": 4,
        "isbn": "",
        "title": "40 Rules of Love",
        "author": "",
        "translated_by": "",
        "img": url_for("static", filename="img/love.jpg"),
    },
    {
        "id": 5,
        "isbn": "",
        "title": "Animal Farm",
        "author": "",
        "translated_by": "",
        "img": url_for("static", filename="img/a_farm.jpg"),
    },
    {
        "id": 6,
        "isbn": "",
        "title": "The Hunchback of Notre-Dame",
        "author": "",
        "translated_by": "",
        "img": url_for("static", filename="img/notr-dame.jpg"),
    },
]


def get_random_book():
    i = random.randint(a=0, b=5)
    return books[i]
