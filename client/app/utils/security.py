from werkzeug.local import LocalProxy
from flask import session

current_user = LocalProxy(lambda: get_current_user())

def get_current_user():
    if (current_user := session.get("email", None)) is None:
        return None
    return current_user