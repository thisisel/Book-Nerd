# TODO move to /app/utils/
"""
Extensions module

Each extension is initialized when app is created.
"""

from flask_assets import Environment
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_session import Session
from flask_wtf import CSRFProtect
from redis import Redis

cors = CORS()
ma = Marshmallow()
assets = Environment()
csrf = CSRFProtect()
r = Redis()
ses = Session()
