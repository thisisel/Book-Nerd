from functools import wraps

from flask import  redirect, render_template, request, session, url_for
from .security import get_current_user


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if (get_current_user()) is None:
            return redirect(url_for("auth_bp.login", next=request.url))
        return f(*args,**kwargs)

    return decorated_function


def templated(template=None):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            template_name = template
            if template_name is None:
                template_name = f"{request.endpoint.replace('.', '/')}.html"
            ctx = f(*args, **kwargs)
            if ctx is None:
                ctx = {}
            elif not isinstance(ctx, dict):
                return ctx
            return render_template(template_name, **ctx)

        return decorated_function

    return decorator

# def cached(timeout=5 * 60, key='view/{}'):
#     def decorator(f):
#         @wraps(f)
#         def decorated_function(*args, **kwargs):
#             cache_key = key.format(request.path)
#             rv = cache.get(cache_key)
#             if rv is not None:
#                 return rv
#             rv = f(*args, **kwargs)
#             cache.set(cache_key, rv, timeout=timeout)
#             return rv
#         return decorated_function
#     return decorator