class FlaskApiUrls:
    BASE = "http://127.0.0.1:5000"

    AUTH = BASE + "/auth"

    API_ROOT = BASE + "/api"


class FastApiUrls:
    BASE = "http://0.0.0.0:8000"

    LOGIN_JWT = BASE + "/auth/jwt/login"
    REGISTER = BASE + "/auth/register"

    API_ROOT = BASE + "/api"
    SHELF_PREFIX = "/shelf"

    PROFILE = API_ROOT + "/profile"
    PROFILE_ME = PROFILE + "/me"
    PERSONAL_SHELF = PROFILE + SHELF_PREFIX

    EXPLORE = API_ROOT + "/explore"
    EXPLORE_BOOK = EXPLORE + "/books"
