from flask import session


class FastApiHeaders:
    FORM_ENCODED = {
        "Content-Type": "application/x-www-form-urlencoded",
        "accept": "application/json",
    }
    MULTIPART_FORM = {
        "Content-Type": "multipart/form-data",
        "accept": "application/json",
        "Authorization": "Bearer ",
    }

    @classmethod
    def get_multipart_authorized(cls):
        return cls.MULTIPART_FORM.update(
            {"Authorization": "Bearer " + session["access_token"]},
        )

    @property
    def multipart_authorized(self):
        return self.MULTIPART_FORM.update(
            {"Authorization": "Bearer " + session["access_token"]},
        )
