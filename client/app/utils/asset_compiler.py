"""Compile static assets."""

from flask import current_app as app
from flask_assets import Bundle


def compile_static_assets(assets):
    """Configure and build asset bundles."""

    # Main Stylesheets Bundle
    main_style_bundle = Bundle(
        "src/css/style.css",
        filters="cssmin",
        output="dist/css/style.min.css",
        extra={"rel": "stylesheet/css"},
    )

    # TODO error style bundle

    # TODO basic search bar bundle
    basic_search_style_bundle = Bundle(
        "src/css/basic-search.css",
        filters="cssmin",
        output="dist/css/basic-search.min.css",
        extra={"rel": "stylesheet/css"},
    )

    # js bundle here
    # TODO compile choices.js

    assets.register("main_styles", main_style_bundle)
    assets.register("basic_search_style", basic_search_style_bundle)

    if app.config["FLASK_ENV"] != "production":
        main_style_bundle.build()
        basic_search_style_bundle.build()

    return assets
