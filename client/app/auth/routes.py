from flask import (
    abort,
    current_app,
    flash,
    redirect,
    render_template,
    request,
    session,
    url_for,
)

from app.schemas import LoginPayloadschema

from . import auth_bp
from .forms import ForgotPasswordForm, LoginForm, RegistrationForm
from .services import AuthService


@auth_bp.route("/register", methods=["GET", "POST"])
def register():
    form = RegistrationForm()
    return render_template("register.html", form=form)


@auth_bp.route("/login", methods=["GET", "POST"])
def login(next=None):

    form = LoginForm()
    if form.validate_on_submit():

        login_payload = LoginPayloadschema(
            username=form.email.data, password=form.password.data,
        )

        try:

            login_result = AuthService.login(data=login_payload)

            if login_result.status:

                flash("ورود موفقیت آمیز", category="success")

                # next = request.args.get("next")

                return (
                    redirect(next)
                    if next
                    else redirect(url_for("home_bp.index"))
                )

            else:

                flash(
                    "یک بار دیگر گذرواژه و یا ایمیل خود را بررسی کنید", "danger",
                )

        except Exception as ex:

            current_app.logger.error(ex)
            abort(500)

    return render_template("login.html", form=form, title="ورود")


@auth_bp.route("/forgot-password", methods=["GET", "POST"])
def forgot_password():
    form = ForgotPasswordForm()
    return render_template("forgot-password.html", form=form)


@auth_bp.route("/password-recovery")
def pass_recovered():
    return render_template("recover.html")
