from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileField
from wtforms import (
    BooleanField,
    IntegerField,
    PasswordField,
    SelectField,
    StringField,
    SubmitField,
    TextAreaField,
)
from wtforms.fields.html5 import DateField
from wtforms.validators import (
    DataRequired,
    Email,
    EqualTo,
    Length,
    ValidationError,
    email_validator,
)


class RegistrationForm(FlaskForm):
    username = StringField(
        "نام کاربری", validators=[DataRequired(), Length(min=2, max=20)],
    )
    email = StringField("ایمیل", validators=[DataRequired(), Email()])
    password = PasswordField("گذرواژه", validators=[DataRequired()])
    confirm_password = PasswordField(
        "تکرار گذرواژه", validators=[DataRequired(), EqualTo("password")],
    )
    submit = SubmitField("ثبت‌نام")
    google_submit = SubmitField("ورود با گوگل")


class LoginForm(FlaskForm):
    email = StringField("ایمیل", validators=[DataRequired(), Email()])
    password = PasswordField("گذرواژه", validators=[DataRequired()])
    remember = BooleanField("فراموشم مکن")
    submit = SubmitField("ورود")


class ForgotPasswordForm(FlaskForm):
    email = StringField("ایمیل", validators=[DataRequired(), Email()])
    submit = SubmitField("ارسال")


class BasicSearchBook(FlaskForm):
    isbn = StringField(
        "ISBN",
        validators=[DataRequired(), Length(min=10, max=13)],
        id="choices-text-preset-values",
    )
    submit = SubmitField("Search")
