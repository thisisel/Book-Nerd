from flask import session


def current_user():
    if(current_user := session.get("email")) is None:
        current_user = 0
    return current_user
