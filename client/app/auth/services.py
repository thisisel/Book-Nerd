import requests
from flask import abort, current_app
from wtforms.validators import Email

from app.schemas import LoginAPISuccessResponse, LoginPayloadschema
from app.schemas.core import InternalMessage
from app.schemas.user import CurrentUserSchema
from app.utils.http_components import FastApiHeaders, FastApiUrls


class AuthService:
    @staticmethod
    def login(data: LoginPayloadschema) -> InternalMessage:

        response = requests.post(
            url=FastApiUrls.LOGIN_JWT,
            data=data.dict(),
            headers=FastApiHeaders.FORM_ENCODED,
        )

        try:

            if response.ok:

                response_json = LoginAPISuccessResponse(**response.json())

                user = CurrentUserSchema(
                    email=data.username, access_token=response_json.access_token,
                )

                return InternalMessage(
                    status=True, code=200, message="login Successful", data=user,
                )

            else:

                response_json = response.json()

                current_app.logger.info(response_json)
                return InternalMessage(
                    status=False,
                    code=response.status_code,
                    message="login failed",
                    data=response_json,
                )

        except Exception as ex:

            current_app.logger.error(ex)
            abort(500)
