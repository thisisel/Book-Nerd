from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileField
from wtforms import (
    BooleanField,
    IntegerField,
    SelectField,
    StringField,
    SubmitField,
    TextAreaField,
)
from wtforms.fields.html5 import DateField
from wtforms.validators import (
    DataRequired,
    Email,
    Length,
    ValidationError,
    email_validator,
)


class UpdateAccountForm(FlaskForm):
    username = StringField(
        "Username", validators=[DataRequired(), Length(min=2, max=20)],
    )
    email = StringField("Email", validators=[DataRequired(), Email()])
    picture = FileField(
        "Update Profile Picture", validators=[FileAllowed(["jpg", "png"])],
    )
    submit = SubmitField("Update")


class BookForm(FlaskForm):
    title = StringField("نام کتاب", validators=[DataRequired()])
    isbn = IntegerField("isbn", validators=[DataRequired()])
    author = StringField("نویسنده", validators=[DataRequired()])
    translated_by = StringField("مترجم", validators=[DataRequired()])
    published_by = StringField("انتشارات", validators=[DataRequired()])
    genre = StringField("ژانر", validators=[DataRequired()])
    cover_description = TextAreaField("شرح", validators=[DataRequired()])
    language = StringField("زبان", validators=[DataRequired()])
    language = SelectField(
        "Language",
        choices=[
            ("english", "English"),
            ("persian", "Persian"),
            ("arabic", "Arabic"),
            ("german", "German"),
            ("french", "French"),
            ("italian", "Italian"),
            ("russian", "Russian"),
            ("other", "other - in the description"),
        ],
        validators=[DataRequired()],
    )
    registered_date = DateField("تاریخ ثبت", validators=[DataRequired()])
    quality = SelectField(
        "کیفیت کتاب",
        choices=[
            ("new", "New"),
            ("second handed - excellent", "Second Handed - Excellent"),
            ("second handed - good", "Second Handed - Good"),
            ("second handed - poor", "Second Handed - Poor"),
        ],
        validators=[DataRequired()],
    )
    img = FileField("بارگزاری  تصویر", validators=[FileAllowed(["jpg", "png"])])
    submit = SubmitField("ثبت")


class ShelfItemForm(FlaskForm):
    title = StringField("نام کتاب", validators=[DataRequired()])
    isbn = StringField("شابک", validators=[DataRequired()])
    registered_date = DateField("تاریخ ثبت", validators=[DataRequired()])
    quality = SelectField(
        "کیفیت کتاب",
        choices=[
            ("new", "New"),
            ("second handed - excellent", "Second Handed - Excellent"),
            ("second handed - good", "Second Handed - Good"),
            ("second handed - poor", "Second Handed - Poor"),
        ],
        validators=[DataRequired()],
    )
    img = FileField("بارگزاری  تصویر", validators=[FileAllowed(["jpg", "png"])])
    is_availabl = BooleanField(label="در دسترس")
    in_public_view = BooleanField(label="نمایش در پروفایل")
    borrow_interval = IntegerField(
        "روزهای مجاز امانت", validators=[DataRequired()],
    )
    submit = SubmitField("ثبت")
