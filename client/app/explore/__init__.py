from flask import Blueprint

explore_bp = Blueprint(
    "explore_bp", __name__, static_folder="static", template_folder="templates",
)
