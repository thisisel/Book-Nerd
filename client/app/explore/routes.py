from flask import (
    abort,
    current_app,
    flash,
    redirect,
    render_template,
    request,
    session,
    url_for,
)

from . import explore_bp


@explore_bp.route("/books")
def search_book():
    isbn = request.args.get("isbn")
    if isbn is None:
        return render_template("basic-search.html")


@explore_bp.route("/books/<string:isbn>")
def book_detail(isbn):
    # TODO send request to real server

    from app.utils.mock_server import get_random_book

    return render_template("book_detail.html", book=get_random_book())


@explore_bp.route("/library")
def search_public_shelf():
    pass


@explore_bp.route("/library/<int:id>")
def shelf_item_detail(id):
    pass


@explore_bp.route("/users")
def search_users():
    pass


@explore_bp.route("/users/<string:username>")
def user_public_profile(username):
    pass
