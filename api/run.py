from pathlib import Path

import uvicorn
from fastapi.staticfiles import StaticFiles

from app import app

Path("static/profile").mkdir(parents=True, exist_ok=True)

app.mount("/static", StaticFiles(directory="static"), name="static")
app.mount(
    "/static/profile",
    StaticFiles(directory="static/profile"),
    name="static_profile",
)


def run():
    uvicorn.run("run:app", host="0.0.0.0", port=8000, reload=True)


if __name__ == "__main__":
    run()
