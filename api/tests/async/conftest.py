import os

from tortoise.contrib.test import finalizer, initializer

from ..components import DB_TEST_URI, app
from . import httpx, pytest


@pytest.fixture
@pytest.mark.asyncio
async def test_async_app(initialize_tests):

    async with httpx.AsyncClient(app=app, base_url="http://test") as ac:
        yield ac


# @pytest.fixture
# @pytest.mark.asyncio
async def test_seed_db(test_async_app: httpx.AsyncClient, db_client):

    from tortoise.backends.base.client import BaseDBAsyncClient

    from ..seed_db import (
        insert_books,
        insert_library,
        insert_requests,
        insert_users,
    )

    # from app.db.models import UserModel, BookModel, LibraryModel, RequestModel
    # UserModel.bulk_create([
    # ])
    # BookModel.bulk_create([
    # ])
    # LibraryModel.bulk_create([
    # ])
    # RequestModel.bulk_create([
    # ])
    db_client = BaseDBAsyncClient(connection_name=DB_TEST_URI)

    await insert_users(db_client=db_client)
    await insert_books()(db_client=db_client)
    await insert_library()(db_client=db_client)
    await insert_requests()(db_client=db_client)

    return db_client


@pytest.fixture
@pytest.mark.asyncio
def initialize_tests(request):
    from app.db.settings import basedir

    db_uri = "sqlite:///" + os.path.join(basedir, "test.sqlite")
    initializer(["app.db.models"], db_url=db_uri, app_label="models")
    yield
    request.addfinalizer(finalizer)
