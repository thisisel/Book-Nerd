from .. import httpx, pytest


@pytest.mark.asyncio
async def test_root(test_async_app: httpx.AsyncClient):

    response = await test_async_app.get("/api")

    assert response.status_code == 200
    assert response.json() == {
        "message": "Welcome to Book-Nerd Api",
        "status": True,
    }
