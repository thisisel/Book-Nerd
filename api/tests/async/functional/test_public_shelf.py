url = "http://0.0.0.0:8000/api/users/username_1/shelf"

from .. import httpx, pytest


@pytest.mark.asyncio
async def test_valid_public_shelf(test_async_app: httpx.AsyncClient):

    response = await test_async_app.get("/api/users/username_1/shelf")

    print(response.text)

    assert response.status_code == 200
    assert response.json() == {
        "message": "Shelf items successfully retrived",
        "status": True,
    }
