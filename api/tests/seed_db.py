from tortoise.backends.base.client import BaseDBAsyncClient


async def insert_books(db_client: BaseDBAsyncClient):

    db_client.execute_query(
        """
                INSERT INTO books (isbn,title,author,translated_by,published_by,genre,cover_description,"language",registered_on,cover_img,accessible,page_number) VALUES
	 ('9780439362139','Harry Potter and the Sorcerer''s Stone','J.K. Rowling','Original','Scholastic Inc','Fantasy ','Harry Potter''s life is miserable. His parents are dead and he''s stuck with his heartless relatives, who force him to live in a tiny closet under the stairs. But his fortune changes when he receives a letter that tells him the truth about himself: he''s a wizard. A mysterious visitor rescues him from his relatives and takes him to his new home, Hogwarts School of Witchcraft','English','2021-06-06 16:24:37','cover-image',1,NULL),
	 ('9780439203531','Harry Potter and the Chamber of Secrets','J.K. Rowling','Original','Scholastic Inc','Fantasy ','Ever since Harry Potter had come home for the summer, the Dursleys had been so mean and hideous that all Harry wanted was to get back to the Hogwarts School for Witchcraft and Wizardry. But just as he’s packing his bags, Harry receives a warning from a strange impish creature who says that if Harry returns to Hogwarts, disaster will strike','English','2021-06-06 16:26:45','cover-image',1,NULL),
	 ('9780439554923','Harry Potter and the Prisoner of Azkaban','J.K. Rowling','Original','Scholastic Inc','Fantasy ','For twelve long years, the dread fortress of Azkaban held an infamous prisoner named Sirius Black. Convicted of killing thirteen people with a single curse, he was said to be the heir apparent to the Dark Lord, Voldemort.','English','2021-06-06 16:29:56','cover-image',1,NULL),
	 ('9781408855928','Harry Potter and the Goblet of Fire','J.K. Rowling','Original','Scholastic Inc','Fantasy ','Harry Potter is midway through his training as a wizard and his coming of age. Harry wants to get away from the pernicious Dursleys and go to the International Quidditch Cup with Hermione, Ron, and the Weasleys. He wants to dream about Cho Chang, his crush (and maybe do more than dream). He wants to find out about the mysterious event that''s supposed to take place at Hogwa','English','2021-06-06 16:29:56','cover-image',1,NULL),
	 ('9781907545139','Harry Potter and the Order of the Phoenix','J.K. Rowling','Original','Scholastic Inc','Fantasy ','There is a door at the end of a silent corridor. And it’s haunting Harry Pottter’s dreams. Why else would he be waking in the middle of the night, screaming in terror?','English','2021-06-06 16:31:50','cover-image',1,NULL),
	 ('9780439791328','Harry Potter and the Half-Blood Prince','J.K. Rowling','Original','Scholastic Inc','Fantasy ','The war against Voldemort is not going well; even Muggle governments are noticing. Ron scans the obituary pages of the Daily Prophet, looking for familiar names. Dumbledore is absent from Hogwarts for long stretches of time, and the Order of the Phoenix has already suffered losses.','English','2021-06-06 16:33:02','cover-image',1,NULL),
	 ('9780545029360','Harry Potter and the Deathly Hallows','J.K. Rowling','Original','Scholastic Inc','Fantasy ','Harry Potter is leaving Privet Drive for the last time. But as he climbs into the sidecar of Hagrid’s motorbike and they take to the skies, he knows Lord Voldemort and the Death Eaters will not be far behind.','English','2021-06-06 16:35:35','cover-image',1,NULL);
            """,
    )


async def insert_users(db_client: BaseDBAsyncClient):

    db_client.execute_query(
        """
                INSERT INTO users (id,email,hashed_password,is_active,is_superuser,is_verified,username,joined_date) VALUES
	 ('26f3ffbd-e519-4606-93e8-7cfc610552bf','user1@example.com','$2b$12$nAE1GKyXHO9.Lu5DDQ0h..wehxgB3KYiZYtpxyp5pGCavhcoLT/jq',1,0,0,'username_1','2021-06-19 11:46:20.535339+00:00'),
	 ('2b933a8e-d653-4274-9a71-5b012bff9ab1','user2@example.com','$2b$12$E70QWbQ0EPPB3ZYGQqcA/eLH4rWJXvQpimtBccB6e0H.1AEOLQ7PG',1,0,0,'username_2','2021-06-19 11:46:34.694830+00:00'),
	 ('0b2574a7-466e-4522-8eba-0c52178930f7','user3@example.com','$2b$12$wc..ZzuI.temv7blBLcpvOMoQ19hHA/9B4nugTHcxVAjBdAI0XMPm',1,0,0,'username_3','2021-06-19 11:46:52.150174+00:00'),
	 ('99040fd9-cc6b-4e80-9db6-e2f21772cf0d','user4@example.com','$2b$12$MSK3ltfzZJx6AuA6LJa3NeTRdQ285mPNE0ZFZyfwxQ4qBYNEDUOiu',1,0,0,'username_4','2021-06-19 11:47:05.326865+00:00'),
	 ('97c66004-40eb-468f-8a9a-168e43a89125','elahemaruf@example.com','$2b$12$/5mz/52bc1rRZ2Bxvkm3wuYgz4BrwRuu2Tl8B69fkpOR18Z7KO9Cq',1,0,0,'iamelahe','2021-06-19 13:07:14.253684+00:00');
            """,
    )


async def insert_library(db_client: BaseDBAsyncClient):

    db_client.execute_query(
        """
            INSERT INTO library (in_public_profile,is_available,verified,borrow_interval,date_added,date_modified,book_id,owner_id) VALUES
	 (0,0,0,18,'2021-06-19 07:22:26','2021-06-19 07:22:26',2,'2b933a8e-d653-4274-9a71-5b012bff9ab1'),
	 (1,0,0,7,'2021-06-19 07:22:26','2021-06-19 07:22:26',6,'99040fd9-cc6b-4e80-9db6-e2f21772cf0d'),
	 (0,0,1,14,'2021-06-19 07:22:26','2021-06-19 07:22:26',3,'0b2574a7-466e-4522-8eba-0c52178930f7'),
	 (0,1,0,11,'2021-06-19 07:22:26','2021-06-19 07:22:26',1,'2b933a8e-d653-4274-9a71-5b012bff9ab1'),
	 (0,0,1,20,'2021-06-19 07:22:26','2021-06-19 07:22:26',7,'0b2574a7-466e-4522-8eba-0c52178930f7'),
	 (0,1,1,7,'2021-06-19 07:22:26','2021-06-19 07:22:26',7,'0b2574a7-466e-4522-8eba-0c52178930f7'),
	 (1,1,1,17,'2021-06-19 07:22:26','2021-06-19 07:22:26',4,'0b2574a7-466e-4522-8eba-0c52178930f7'),
	 (1,0,0,14,'2021-06-19 07:22:26','2021-06-19 07:22:26',5,'2b933a8e-d653-4274-9a71-5b012bff9ab1'),
	 (0,1,0,9,'2021-06-19 07:22:26','2021-06-19 07:22:26',5,'2b933a8e-d653-4274-9a71-5b012bff9ab1'),
	 (1,0,1,11,'2021-06-19 07:22:26','2021-06-19 07:22:26',3,'0b2574a7-466e-4522-8eba-0c52178930f7');
        """,
    )
    db_client.execute_query(
        """
            INSERT INTO library (in_public_profile,is_available,verified,borrow_interval,date_added,date_modified,book_id,owner_id) VALUES
	 (0,0,0,17,'2021-06-19 07:22:26','2021-06-19 07:22:26',5,'26f3ffbd-e519-4606-93e8-7cfc610552bf'),
	 (1,0,1,13,'2021-06-19 07:22:26','2021-06-19 07:22:26',2,'26f3ffbd-e519-4606-93e8-7cfc610552bf'),
	 (1,1,1,9,'2021-06-19 07:22:26','2021-06-19 07:22:26',1,'0b2574a7-466e-4522-8eba-0c52178930f7'),
	 (1,0,1,14,'2021-06-19 07:22:26','2021-06-19 07:22:26',2,'0b2574a7-466e-4522-8eba-0c52178930f7'),
	 (0,0,1,8,'2021-06-19 07:22:26','2021-06-19 07:22:26',3,'26f3ffbd-e519-4606-93e8-7cfc610552bf'),
	 (0,0,0,9,'2021-06-19 07:22:26','2021-06-19 07:22:26',2,'26f3ffbd-e519-4606-93e8-7cfc610552bf'),
	 (1,1,1,6,'2021-06-19 07:22:26','2021-06-19 07:22:26',2,'26f3ffbd-e519-4606-93e8-7cfc610552bf'),
	 (1,1,0,9,'2021-06-19 07:22:26','2021-06-19 07:22:26',7,'99040fd9-cc6b-4e80-9db6-e2f21772cf0d'),
	 (0,0,1,7,'2021-06-19 07:22:26','2021-06-19 07:22:26',4,'26f3ffbd-e519-4606-93e8-7cfc610552bf'),
	 (1,0,0,6,'2021-06-19 07:22:26','2021-06-19 07:22:26',4,'2b933a8e-d653-4274-9a71-5b012bff9ab1');

        """,
    )


async def insert_requests(db_client: BaseDBAsyncClient):

    db_client.execute_query(
        """
        INSERT INTO requests (status,submitted,last_modified,note,requested_by_id,requested_item_id) VALUES
	 (2,'2021-06-19 07:25:36','2021-06-19 07:25:36','dolor in reprehenderit in voluptate veli','26f3ffbd-e519-4606-93e8-7cfc610552bf',86),
	 (2,'2021-06-19 07:25:36','2021-06-19 07:25:36','sint occaecat cupidatat ','26f3ffbd-e519-4606-93e8-7cfc610552bf',93),
	 (2,'2021-06-19 07:25:36','2021-06-19 07:25:36','et d','26f3ffbd-e519-4606-93e8-7cfc610552bf',65),
	 (2,'2021-06-19 07:25:36','2021-06-19 07:25:36','tempor incididunt ut labore et dolore magna aliqua. U','26f3ffbd-e519-4606-93e8-7cfc610552bf',65),
	 (2,'2021-06-19 07:25:36','2021-06-19 07:25:36','sunt in culpa qui officia deserunt mollit anim','26f3ffbd-e519-4606-93e8-7cfc610552bf',86),
	 (2,'2021-06-19 07:25:36','2021-06-19 07:25:36','in culpa qui officia deserunt mollit a','99040fd9-cc6b-4e80-9db6-e2f21772cf0d',8),
	 (2,'2021-06-19 07:25:36','2021-06-19 07:25:36','do eiusmod tempor incididunt ut ','2b933a8e-d653-4274-9a71-5b012bff9ab1',32),
	 (2,'2021-06-19 07:25:36','2021-06-19 07:25:36','tempor incididunt ut labore et dolore magna aliqua. Ut enim ad mi','26f3ffbd-e519-4606-93e8-7cfc610552bf',32),
	 (2,'2021-06-19 07:25:36','2021-06-19 07:25:36','mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing','2b933a8e-d653-4274-9a71-5b012bff9ab1',87),
	 (2,'2021-06-19 07:25:36','2021-06-19 07:25:36','nulla pariatur. Except','2b933a8e-d653-4274-9a71-5b012bff9ab1',32);
        """,
    )
    db_client.execute_query(
        """
        INSERT INTO requests (status,submitted,last_modified,note,requested_by_id,requested_item_id) VALUES
	 (2,'2021-06-19 09:37:55.374359+00:00','2021-06-19 09:37:55.374415+00:00','string','97c66004-40eb-468f-8a9a-168e43a89125',59),
	 (2,'2021-06-19 11:05:06.252769+00:00','2021-06-19 11:05:06.252851+00:00','string','97c66004-40eb-468f-8a9a-168e43a89125',87),
	 (2,'2021-06-19 11:50:43.500820+00:00','2021-06-19 11:50:43.500871+00:00','string','97c66004-40eb-468f-8a9a-168e43a89125',87);
        """,
    )
