from ..enums import RequestStatus


class RequestIssuerPermissions:

    # key: current status value: updatable status
    update_status = dict(
        ACCEPTED=5,  # RequestStatus.CANCELED.value
        PENDING=5,  # RequestStatus.CANCELED.value
        CANCELED=5,  # RequestStatus.CANCELED.value
    )

    @classmethod
    async def tuple_status_is_updatable(
        cls, current_status: int, update_value: int,
    ) -> bool:
        i = cls.update_status.get(RequestStatus(current_status).name)

        if i != update_value:
            return False
        return True

    @classmethod
    async def is_updatable(cls, current_status: int, update_value: int) -> bool:
        # return True if current_status in (RequestStatus.ACCEPTED.value, RequestStatus.PENDING.value, RequestStatus.CANCELED.value) and update_value == RequestStatus.CANCELED.value else False
        return (
            True if current_status in (1, 2, 5) and update_value == 5 else False
        )


class RequestReceiverPermissions:

    # key: current status value: updatable status
    update_status = dict(
        REJECTED=(
            RequestStatus.ACCEPTED.value
        ),  # RequestStatus.ACCEPTED.value 1
        ACCEPTED=(
            RequestStatus.REJECTED.value
        ),  # RequestStatus.REJECTED.value 0
        PENDING=(
            RequestStatus.REJECTED.value,
            RequestStatus.ACCEPTED.value,
        ),  # RequestStatus.REJECTED.value | RequestStatus.ACCEPTED.value
    )

    @classmethod
    async def is_updatable(cls, current_status: int, update_value: int) -> bool:

        if (
            res := cls.update_status.get(RequestStatus(current_status).name)
        ) is None:
            # invalid status passed
            return False

        if update_value not in res:
            # not authorized
            return False

        return True
