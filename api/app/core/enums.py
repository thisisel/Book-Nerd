from enum import Flag


class RequestStatus(Flag):
    REJECTED: int = 0
    ACCEPTED: int = 1
    PENDING: int = 2
    CLOSED: int = 3
    CANCELED: int = 5


status = dict(
    REJECTED=0,
    ACCEPTED=1,
    PENDING=2,
    CLOSED=3,
    CANCELED=5,
)
