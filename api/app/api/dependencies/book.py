from app.api.errors import BOOK_404, NotFound
from app.crud.book import BookRetrieve
from app.schemes.shelf import CreateShelfItem_Pydantic


class BookExists:
    async def __init__(self, data: CreateShelfItem_Pydantic) -> None:
        book_qset = await BookRetrieve.fetch_single_book(
            id=data.dict().get("book_id"),
        )
        if (book := await book_qset) is None:
            raise NotFound(category=BOOK_404)

        return book
