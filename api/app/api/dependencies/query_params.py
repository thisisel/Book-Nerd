from typing import Optional, Set

from fastapi import Query, Request
from tortoise.query_utils import Q

from app.core.log.current_logger import CurrentLogger


class PersonalShelfFilters:
    def __init__(
        self,
        request: Request,
        is_available: Optional[bool] = Query(None),
        borrow_interval_gte: Optional[int] = Query(None),
        borrow_interval_lte: Optional[int] = Query(None),
        title: Optional[str] = Query(None),
        verified: Optional[bool] = Query(None),
        in_public_profile: Optional[bool] = Query(None),
    ):
        self._q_filters = dict(
            title=Q(book__title__icontains=title),
            is_available=Q(is_available=is_available),
            borrow_interval_gte=Q(borrow_interval__gte=borrow_interval_gte),
            borrow_interval_lte=Q(borrow_interval__lte=borrow_interval_lte),
            verified=Q(verified=verified),
            in_public_profile=Q(in_public_profile=in_public_profile),
        )

        self.q_filters_pruned: Set[Q] = {
            q_object
            for param, q_object in self._q_filters.items()
            if request.query_params.get(param) is not None
        }

        CurrentLogger.get_logger().debug(self.q_filters_pruned)


class PublicShelfFilters(PersonalShelfFilters):
    def __init__(
        self,
        request: Request,
        is_available: Optional[bool] = Query(None),
        borrow_interval_gte: Optional[int] = Query(None),
        borrow_interval_lte: Optional[int] = Query(None),
        title: Optional[str] = Query(None),
    ):
        super().__init__(
            request,
            is_available=is_available,
            borrow_interval_gte=borrow_interval_gte,
            borrow_interval_lte=borrow_interval_lte,
            title=title,
            verified=True,
            in_public_profile=True,
        )
