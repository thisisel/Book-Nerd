from typing import Optional

from fastapi import Form
from fastapi_users import user
from pydantic import EmailStr

from app.core.security.auth import UsersAuth

current_active_user = UsersAuth.get_fastapiusers().current_user(active=True)


class UserUpdateForm:
    def __init__(
        self,
        email: EmailStr = Form(None),
        username: Optional[str] = Form(None),
        firstname: Optional[str] = Form(None),
        lastname: Optional[str] = Form(None),
        phonenumber: Optional[str] = Form(None),
        ostan_id: Optional[int] = Form(None),
        shahr_id: Optional[int] = Form(None),
    ) -> None:
        self.email = email
        self.username = username
        self.firstname = firstname
        self.lastname = lastname
        self.phonenumber = phonenumber
        self.ostan_id = ostan_id
        self.shahr_id = shahr_id
