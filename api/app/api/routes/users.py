from fastapi.routing import APIRouter

from .shelf import public_shelf

router = APIRouter()
router.include_router(
    public_shelf.router, prefix="/{username}/shelf", tags=["public shelf"],
)


@router.get("/{username}", tags=["public profile"])
async def get_user_info():
    pass
