from fastapi.encoders import jsonable_encoder
from fastapi.params import Body, Depends, Param, Path
from fastapi.routing import APIRouter
from fastapi_pagination.default import Page, Params
from fastapi_pagination.paginator import paginate
from starlette.responses import JSONResponse

from app.api.errors import OUTBOX_REQUEST_404, NotFound
from app.crud.request import RetrieveRequest, UpdateRequest
from app.schemes.common_models import ApiErrorResponse
from app.schemes.request import (
    BookRequestApiResponse,
    Outbox_Pydantic,
    Request_Pydantic,
    UpdateRequestIssuerSide,
)
from app.schemes.user import UserDB

from ..dependencies.user import current_active_user

router = APIRouter()


@router.get("/", response_model=Page[Outbox_Pydantic])
async def retrive_outbox(
    user: UserDB = Depends(current_active_user), params: Params = Depends(),
):

    outbox = await RetrieveRequest.fetch_outbox(user_id=user.id)
    return paginate(outbox, params)


@router.get(
    "/{id}",
    responses={
        200: {"model": BookRequestApiResponse},
        404: {"model": ApiErrorResponse},
    },
)
async def retrive_outbox_request(
    id: int = Param(...), user: UserDB = Depends(current_active_user),
):

    outbox_request = await RetrieveRequest.fetch_outbox_request(
        user_id=user.id, outbox_id=id,
    )

    content = BookRequestApiResponse(
        status=True,
        message="Request successfully retrived from user's outbox",
        data=await Request_Pydantic.from_tortoise_orm(outbox_request),
    )

    return JSONResponse(status_code=200, content=jsonable_encoder(content))


@router.patch(
    "/{id}",
    responses={
        200: {"model": BookRequestApiResponse},
        404: {"model": ApiErrorResponse},
    },
)
async def update_request(
    id: int = Path(...),
    user: UserDB = Depends(current_active_user),
    data: UpdateRequestIssuerSide = Body(...),
):

    # TODO check status update value in 0,1 and 3
    updated_outbox_request = await UpdateRequest.update_request(
        request_id=id, user_id=user.id, data=data,
    )

    content = BookRequestApiResponse(
        status=True,
        message="Outbox Request successfully updated",
        data=await Request_Pydantic.from_tortoise_orm(updated_outbox_request),
    )

    return JSONResponse(status_code=200, content=jsonable_encoder(content))
