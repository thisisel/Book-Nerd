from fastapi.encoders import jsonable_encoder
from fastapi.param_functions import Body
from fastapi.params import Depends, Param
from fastapi.routing import APIRouter
from fastapi_pagination import Page, Params, paginate
from starlette.responses import JSONResponse

from app.crud.request import RetrieveRequest, UpdateRequest
from app.schemes.common_models import ApiErrorResponse
from app.schemes.request import (
    BookRequestApiResponse,
    Inbox_Pydantic,
    Request_Pydantic,
    UpdateRequestReceiverSide,
)
from app.schemes.user import UserDB

from ..dependencies.user import current_active_user

router = APIRouter()


@router.get("/", response_model=Page[Inbox_Pydantic])
async def retrieve_inbox(
    user: UserDB = Depends(current_active_user), params: Params = Depends(),
):
    return paginate(await RetrieveRequest.fetch_inbox(user_id=user.id), params)


@router.get(
    "/{id}",
    responses={
        200: {"model": BookRequestApiResponse},
        404: {"model": ApiErrorResponse},
    },
)
async def retrieve_inbox_request(
    id: int, user: UserDB = Depends(current_active_user),
):

    inbox_request = await RetrieveRequest.fetch_inbox_request(
        user_id=user.id, inbox_id=id,
    )

    content = BookRequestApiResponse(
        status=True,
        message="Request successfully retrived from user's inbox",
        data=await Inbox_Pydantic.from_tortoise_orm(inbox_request),
    )

    return JSONResponse(status_code=200, content=jsonable_encoder(content))


@router.patch(
    "/{id}",
    responses={
        200: {"model": BookRequestApiResponse},
        404: {"model": ApiErrorResponse},
    },
)
async def update_inbox_request(
    id: int = Param(...),
    user: UserDB = Depends(current_active_user),
    data: UpdateRequestReceiverSide = Body(...),
):

    # TODO check status update value in 0,1 and 3

    updated_inbox_request = await UpdateRequest.update_request(
        request_id=id, user_id=user.id, data=data,
    )
    content = BookRequestApiResponse(
        status=True,
        message="Inbox Request successfully updated",
        data=await Request_Pydantic.from_tortoise_orm(updated_inbox_request),
    )

    return JSONResponse(status_code=200, content=jsonable_encoder(content))
