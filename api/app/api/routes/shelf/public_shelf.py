from fastapi import APIRouter, Body, Depends, HTTPException, Path
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

from app.api.dependencies.user import current_active_user
from app.api.errors import InternalError
from app.core.log.current_logger import CurrentLogger
from app.crud import RetrieveShelf
from app.crud.user import RetrieveUser
from app.schemes.common_models import ApiBaseResponse, ApiErrorResponse
from app.schemes.request import (
    BookRequestApiResponse,
    CreateRequestModel,
    Outbox_Pydantic,
)
from app.schemes.shelf import (
    PublicShelfItemsResponse,
    ShelfItemPubic_Pydantic,
    ShelfItemPublicResponse,
)
from app.schemes.user import UserDB
from app.utils.shelf import ShelfItemUtils

router = APIRouter()


@router.get(
    "",
    responses={
        200: {"model": PublicShelfItemsResponse},
        404: {"model": ApiBaseResponse},
        500: {"model": ApiBaseResponse},
    },
    description="Get all the items that a user has shared for public view",
)
async def get_user_public_shelf(username: str = Path(...)):

    user = await RetrieveUser.fetch_user_by_username(username=username)

    try:

        user_shelf = await RetrieveShelf.fetch_user_public_shelf_list(user=user)
        content = await ShelfItemUtils.compose_public_shelf_items_response(
            owner=jsonable_encoder(user), shelf=user_shelf,
        )

        return JSONResponse(status_code=200, content=jsonable_encoder(content))

    except Exception as ex:

        CurrentLogger.get_logger().error(ex)
        return InternalError()


@router.get(
    "/{id}",
    responses={
        200: {"model": ShelfItemPublicResponse},
        404: {"model": ApiErrorResponse},
        500: {"model": ApiErrorResponse},
    },
)
async def get_public_shelf_item(username: str = Path(...), id: int = Path(...)):

    user = await RetrieveUser.fetch_user_by_username(username=username)
    item = await RetrieveShelf.fetch_single_public_item(
        item_id=id, owner_id=user.id,
    )

    content = ShelfItemPublicResponse(
        status=True,
        message="Public data retrived successfully",
        item=await ShelfItemPubic_Pydantic.from_tortoise_orm(item),
    )

    return JSONResponse(status_code=200, content=jsonable_encoder(content))


@router.post(
    "/{id}",
    tags=["outbox"],
    responses={
        201: {"model": BookRequestApiResponse},
        406: {"model": ApiErrorResponse},
        404: {"model": ApiErrorResponse},
    },
    status_code=201,
)
async def send_borrow_request(
    send_by: UserDB = Depends(current_active_user),
    username: str = Path(...),
    data: CreateRequestModel = Body(...),
    id: int = Path(..., description="Shelf Item ID"),
):
    # TODO no duplicate request while deal is pending or ongoing

    receiver = await RetrieveUser.fetch_user_by_username(username=username)
    item = await RetrieveShelf.fetch_single_public_item(
        item_id=id, owner_id=receiver.id,
    )

    if item.id != data.dict().get("requested_item_id"):

        error = ApiBaseResponse(
            status=False,
            category="bad_request_400",
            message="Requested item id does not match with resource",
        )

        raise HTTPException(status_code=400, detail=jsonable_encoder(error))

    try:

        from app.crud import CreateRequest

        request_created = await CreateRequest.create_new_request(
            data=data,
            send_by=send_by,
        )
        content = BookRequestApiResponse(
            status=True,
            message="New request created successfully",
            data=await Outbox_Pydantic.from_tortoise_orm(request_created),
        )

        return JSONResponse(status_code=201, content=jsonable_encoder(content))

    except Exception as ex:

        CurrentLogger.get_logger().error(ex)
        raise InternalError()
