from typing import List

from fastapi import APIRouter, Body, Depends, Path
from fastapi.encoders import jsonable_encoder
from fastapi_pagination.default import Page, Params
from fastapi_pagination.paginator import paginate
from starlette.responses import JSONResponse

from app.api.dependencies.query_params import PersonalShelfFilters
from app.api.errors import (
    BOOK_404,
    OWNER_AUTH,
    Forbidden,
    InternalError,
    NotFound,
)
from app.crud.book import BookRetrieve
from app.crud.shelf import CreateShelf, DeleteShelf, RetrieveShelf, UpdateShelf
from app.schemes.book import BookInShelf_Pydantic
from app.schemes.common_models import ApiErrorResponse
from app.schemes.shelf import (
    CreateShelfItem_Pydantic,
    PrivateShelfItemResponse,
    ShelfItemInProfile_Pydantic,
)
from app.schemes.user import UserDB

from ...dependencies.user import current_active_user

router = APIRouter()


@router.get("/", response_model=Page[ShelfItemInProfile_Pydantic])
async def get_personal_shelf(
    user: UserDB = Depends(current_active_user),
    params: Params = Depends(),
    q_filters: PersonalShelfFilters = Depends(),
):

    shelf_qset = await RetrieveShelf.fetch_private_shelf(
        user_id=user.id, q_filters=q_filters.q_filters_pruned,
    )
    shelf_obj = await ShelfItemInProfile_Pydantic.from_queryset(shelf_qset)
    return paginate(shelf_obj, params)


@router.get(
    "/{id}",
    responses={
        200: {"model": PrivateShelfItemResponse},
        404: {"model": ApiErrorResponse},
    },
)
async def get_personal_shelf_item(
    id: int = Path(...), user: UserDB = Depends(current_active_user),
):

    private_item = await RetrieveShelf.fetch_private_item(
        user_id=user.id, item_id=id,
    )
    book = await private_item.book

    content = PrivateShelfItemResponse(
        status=True,
        message="Shelf item successfully retrived",
        item=await ShelfItemInProfile_Pydantic.from_tortoise_orm(private_item),
        book=await BookInShelf_Pydantic.from_tortoise_orm(book),
    )

    return JSONResponse(
        status_code=200, content=jsonable_encoder(content, exclude_none=True),
    )


@router.post(
    "/",
    responses={
        201: {"model": PrivateShelfItemResponse},
        400: {"model": ApiErrorResponse},
    },
    status_code=201,
)
async def add_shelf_item(
    data: CreateShelfItem_Pydantic = Body(...),
    user: UserDB = Depends(current_active_user),
):

    if not (await BookRetrieve.book_exists(book_id=data.dict().get("book_id"))):
        raise NotFound(category=BOOK_404)

    try:

        new_item = await CreateShelf.create_item(data=data, owner_id=user.id)
        content = PrivateShelfItemResponse(
            status=True,
            message="New item added to personal shelf",
            item=await ShelfItemInProfile_Pydantic.from_tortoise_orm(new_item),
        )

        return JSONResponse(status_code=201, content=jsonable_encoder(content))

    except Exception:
        raise InternalError()


@router.put(
    "/{id}",
    responses={
        200: {"model": PrivateShelfItemResponse},
        404: {"model": ApiErrorResponse},
    },
)
async def update_shelf_item(
    id: int = Path(...),
    user: UserDB = Depends(current_active_user),
    data: CreateShelfItem_Pydantic = Body(...),
):

    book_qset = await BookRetrieve.fetch_single_book(
        id=data.dict().get("book_id"),
    )
    if (book := await book_qset) is None:
        raise NotFound(category=BOOK_404)

    updated_item = await UpdateShelf.update_item(
        item_id=id, user_id=user.id, data=data,
    )
    content = PrivateShelfItemResponse(
        status=True,
        message="Shelf item successfully updated",
        item=await ShelfItemInProfile_Pydantic.from_tortoise_orm(updated_item),
        book=await BookInShelf_Pydantic.from_tortoise_orm(book),
    )

    return JSONResponse(status_code=200, content=jsonable_encoder(content))


@router.delete(
    "/{id}",
    description="Delete an item in user's personal shelf",
    status_code=204,
    response_description="Successfully deleted",
    responses={
        404: {"model": ApiErrorResponse},
    },
)
async def remove_item(
    id: int = Path(...), user: UserDB = Depends(current_active_user),
):

    return await DeleteShelf.remove_shelf_item(user_id=user.id, item_id=id)
