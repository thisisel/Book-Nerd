from typing import Optional

from fastapi import APIRouter, BackgroundTasks, Depends, File, UploadFile

from app.core.security.auth import UsersAuth
from app.crud.user import UpdateUser
from app.schemes.common_models import ApiErrorResponse
from app.schemes.user import UserDB
from app.utils.file_utils import get_picture_path, manipulate_save_thumbnail

from ..dependencies.user import UserUpdateForm, current_active_user
from . import inbox, outbox
from .shelf import private_shelf

router = APIRouter()
router.include_router(
    private_shelf.router, prefix="/shelf", tags=["personal shelf"],
)
router.include_router(outbox.router, prefix="/outbox", tags=["outbox"])
router.include_router(inbox.router, prefix="/inbox", tags=["inbox"])


@router.get(
    "/me",
    tags=["profile"],
    response_model=UserDB,
    response_model_exclude={
        "hashed_password",
    },
)
async def get_my_profile(user: UserDB = Depends(current_active_user)):
    return user


@router.patch("/me", tags=["profile"])
async def update_my_profile(
    background_tasks: BackgroundTasks,
    form: UserUpdateForm = Depends(),
    profile_img: Optional[UploadFile] = File(None),
    user: UserDB = Depends(current_active_user),
):
    picture_path = None
    if profile_img:
        picture_path = get_picture_path(file=profile_img, root_path="")
        background_tasks.add_task(
            manipulate_save_thumbnail,
            file=profile_img.file,
            picture_path=picture_path,
        )

    user_obj_updated = await UpdateUser.update_user(
        user_id=user.id, form=form, profile_img_path=picture_path,
    )

    return await UserDB.from_tortoise_orm(user_obj_updated)
