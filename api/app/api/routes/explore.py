from fastapi.encoders import jsonable_encoder
from fastapi.routing import APIRouter
from tortoise.query_utils import Prefetch, Q

from app.db.models import BookModel, LibraryModel
from app.schemes.shelf import ShelfItemPubic_Pydantic

from . import users

router = APIRouter()
router.include_router(users.router, prefix="/users")


@router.get("/books/")
async def retrive_book(isbn: str):
    from app.core.log.current_logger import CurrentLogger

    # return await BookInPublicShelf_Pydantic.from_queryset_single(BookModel.filter(Q(isbn=isbn) & Q(in_public_profile=True) & Q(verified=True)).first())
    # return await BookInPublicShelf_Pydantic.from_queryset_single(BookModel.filter(isbn=isbn).first())

    book = await BookModel.filter(isbn=isbn).prefetch_related(
        Prefetch(
            "copies",
            queryset=LibraryModel.filter(
                Q(in_public_profile=True) & Q(verified=True),
            ),
        ),
    )
    CurrentLogger.get_logger().info(book)
    CurrentLogger.get_logger().info(jsonable_encoder(book))

    return jsonable_encoder(book)


@router.get("/library")
async def retrieve_recent_shelf_items():
    return await ShelfItemPubic_Pydantic.from_queryset(
        LibraryModel.filter(Q(in_public_profile=True) & Q(verified=True))
        .order_by("date_added")
        .all(),
    )
