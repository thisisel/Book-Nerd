from fastapi import APIRouter

from app.schemes.common_models import ApiBaseResponse

from . import explore, profile, users

router = APIRouter()
router.include_router(profile.router, prefix="/profile")
router.include_router(explore.router, prefix="/explore", tags=["explore"])
# router.include_router(users.router, prefix="/users", tags=["public profile"])


@router.get("/", response_model=ApiBaseResponse, tags=["API root"])
async def root():
    return ApiBaseResponse(status=True, message="Welcome to Book-Nerd Api")
