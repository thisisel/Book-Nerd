from fastapi_users.db import TortoiseBaseUserModel
from tortoise import fields, models
from tortoise.exceptions import NoValuesFetched
from tortoise.fields.base import CASCADE, RESTRICT, SET_NULL
from tortoise.validators import MinLengthValidator


class UserModel(TortoiseBaseUserModel):

    username = fields.CharField(
        max_length=20, index=True, unique=True, null=True,
    )
    joined_date = fields.DatetimeField(auto_now_add=True, null=False)
    firstname = fields.CharField(max_length=100, null=True)
    lastname = fields.CharField(max_length=100, null=True)
    phonenumber = fields.CharField(max_length=11, unique=True, null=True)
    profile_img_path = fields.CharField(max_length=150, null=True)

    shelf: fields.ReverseRelation["LibraryModel"]
    outbox: fields.ReverseRelation["RequestModel"]

    ostan: fields.ForeignKeyRelation["OstanModel"] = fields.ForeignKeyField(
        "models.OstanModel",
        related_name="users",
        on_delete=SET_NULL,
        null=True,
        default=None,
    )
    shahr: fields.ForeignKeyRelation["ShahrModel"] = fields.ForeignKeyField(
        "models.ShahrModel",
        related_name="users",
        on_delete=SET_NULL,
        null=True,
        default=None,
    )

    def ostan_label(self) -> str:
        return self.ostan.name

    def shahr_label(self) -> str:
        return self.shahr.name

    class Meta:
        app = "models"
        table = "users"
        computed = ("profile_img_url",)

    class PydanticMeta:
        exclude = (
            "hashed_password",
            "is_active",
            "is_superuser",
            "is_verified",
            "joined_date",
            "shelf",
            "inbox",
            "outbox",
            "ostan",
            "shahr",
        )


class BookModel(models.Model):
    """Book entities verified by system admin"""

    id = fields.IntField(pk=True)
    isbn = fields.CharField(
        max_length=13,
        null=False,
        unique=True,
        index=True,
        validators=[MinLengthValidator(10)],
    )
    title = fields.CharField(max_length=80, null=False, index=True)
    author = fields.CharField(max_length=50, null=False)
    translated_by = fields.CharField(max_length=50, null=False)
    published_by = fields.CharField(max_length=50, null=True)
    genre = fields.CharField(max_length=10, null=False)
    cover_description = fields.TextField()
    language = fields.CharField(max_length=10, null=False)
    registered_on = fields.DatetimeField(auto_now_add=True)
    cover_img = fields.CharField(
        max_length=250, null=False, default="cover-image",
    )
    accessible = fields.BooleanField(default=True, null=False)
    page_number = fields.IntField(null=True)

    copies: fields.ReverseRelation["LibraryModel"]

    class Meta:
        app = "models"
        table = "books"

    class PydanticMeta:
        exclude = (
            "registered_on",
            "accessible",
        )


class LibraryModel(models.Model):
    """Association model for book items in users's shelf"""

    id = fields.IntField(pk=True)
    in_public_profile = fields.BooleanField(default=False, null=False)
    is_available = fields.BooleanField(default=True, null=False)
    verified = fields.BooleanField(default=False, null=False)
    borrow_interval = fields.IntField(null=False, default=5)
    date_added = fields.DatetimeField(auto_now_add=True)
    date_modified = fields.DatetimeField(auto_now=True)

    owner: fields.ForeignKeyRelation[UserModel] = fields.ForeignKeyField(
        "models.UserModel", related_name="shelf", on_delete=CASCADE,
    )
    book: fields.ForeignKeyRelation[BookModel] = fields.ForeignKeyField(
        "models.BookModel", related_name="copies", on_delete=RESTRICT,
    )

    requests: fields.ReverseRelation["RequestModel"]

    def title(self) -> str:

        try:
            return self.book.title

        except NoValuesFetched:
            return "No Title"

    class Meta:
        app = "models"
        table = "library"
        ordering = ["date_modified"]

    class PydanticMeta:
        exclude = ("requests",)


class RequestModel(models.Model):
    """Association model for resquesting to borrow shelf items"""

    id = fields.IntField(pk=True)
    status = fields.IntField(
        null=False,
        default=2,
        description="0: rejected  1: accepted  2: pending",
    )
    submitted = fields.DatetimeField(auto_now_add=True)
    last_modified = fields.DatetimeField(auto_now=True)
    note = fields.TextField(null=True)

    requested_by: fields.ForeignKeyRelation[UserModel] = fields.ForeignKeyField(
        "models.UserModel", related_name="outbox", on_delete=CASCADE,
    )

    requested_item: fields.ForeignKeyRelation[
        LibraryModel
    ] = fields.ForeignKeyField(
        "models.LibraryModel", related_name="requests", on_delete=RESTRICT,
    )

    class Meta:
        app = "models"
        table = "requests"
        ordering = ["last_modified"]
        unique_together = (("id", "requested_by_id", "requested_item_id"),)

    class PydanticMeta:
        exclude = ("note",)


class ResidenceBaseModel(models.Model):

    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255, null=False)

    users: fields.ReverseRelation["UserModel"]

    class Meta:
        app = "models"
        abstract = True


class OstanModel(ResidenceBaseModel):

    shahrha: fields.ReverseRelation["ShahrModel"]

    class Meta:
        app = "models"
        table = "ostan"


class ShahrModel(ResidenceBaseModel):

    ostan: fields.ForeignKeyRelation[OstanModel] = fields.ForeignKeyField(
        "models.OstanModel", related_name="shahrha", on_delete=CASCADE,
    )

    class Meta:
        app = "models"
        table = "shahr"
