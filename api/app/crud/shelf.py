from typing import List, Set

from fastapi.encoders import jsonable_encoder
from pypika.functions import ToChar
from tortoise.exceptions import OperationalError
from tortoise.query_utils import Prefetch, Q
from tortoise.queryset import QuerySet

from app.api.dependencies import user
from app.api.errors.error_categories import SHELF_ITEM_404
from app.api.errors.internal_error import InternalError
from app.api.errors.not_found_error import NotFound
from app.core.log.current_logger import CurrentLogger
from app.db.models import BookModel, LibraryModel, UserModel
from app.schemes.shelf import CreateShelfItem_Pydantic

from .user import RetrieveUser


class RetrieveShelf:
    @staticmethod
    async def fetch_user_public_shelf_list(
        user: UserModel,
    ) -> List[LibraryModel]:

        return await user.shelf.filter(
            Q(verified=True) & Q(in_public_profile=True),
        )

    @staticmethod
    async def fetch_single_public_item(
        item_id: int, owner_id: int,
    ) -> LibraryModel:

        if (
            public_item := await LibraryModel.filter(
                Q(id=item_id)
                & Q(owner=owner_id)
                & Q(in_public_profile=True)
                & Q(verified=True),
            )
            # .only(
            #     "id",
            #     "borrow_interval",
            #     "is_available",
            #     "date_added",
            #     "date_modified",
            #     "book_id",
            # )
            .first()
            # .values()
        ) is None:
            raise NotFound(category=SHELF_ITEM_404)

        return public_item

    @classmethod
    async def fetch_private_shelf(
        cls, user_id: int, q_filters: Set[Q],
    ) -> QuerySet[LibraryModel]:

        shelf_qset = LibraryModel.filter(Q(owner_id=user_id, *q_filters)).all()
        return shelf_qset

    @classmethod
    async def fetch_private_item(
        cls, user_id: int, item_id: int,
    ) -> LibraryModel:

        user = await RetrieveUser.fetch_user_by_id(user_id=user_id)

        if (
            private_item := await user.shelf.filter(id=item_id).first()
        ) is None:

            raise NotFound(category=SHELF_ITEM_404)

        return private_item


class CreateShelf:
    @classmethod
    async def create_item(
        cls, data: CreateShelfItem_Pydantic, owner_id: int,
    ) -> LibraryModel:

        return await LibraryModel.create(
            **data.dict(exclude_unset=True), owner_id=owner_id
        )


class UpdateShelf:
    @classmethod
    async def update_item(
        cls, user_id: int, item_id: int, data: CreateShelfItem_Pydantic,
    ) -> LibraryModel:

        item = await RetrieveShelf.fetch_private_item(
            user_id=user_id, item_id=item_id,
        )

        await item.update_from_dict(data.dict(exclude_unset=True)).save()

        return item


class DeleteShelf:
    @classmethod
    async def remove_shelf_item(cls, user_id: int, item_id: int) -> None:

        item = await RetrieveShelf.fetch_private_item(
            user_id=user_id, item_id=item_id,
        )

        try:
            await item.delete()

        except OperationalError as op_err:

            CurrentLogger.get_logger().error(op_err)
            raise InternalError()
