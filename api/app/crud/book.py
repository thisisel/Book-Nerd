from typing import Union

from tortoise.queryset import QuerySetSingle

from app.db.models import BookModel


class BookRetrieve:
    @classmethod
    async def fetch_single_book(
        cls, id,
    ) -> Union[QuerySetSingle[BookModel], None]:
        return BookModel.get_or_none(id=id)

    @classmethod
    async def book_exists(cls, book_id) -> bool:
        return await BookModel.exists(id=book_id)
