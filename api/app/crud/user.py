from app.api.dependencies.user import UserUpdateForm
from app.api.errors import USER_404, NotFound
from app.db.models import UserModel
from app.schemes.user import UserDB


class RetrieveUser:
    @staticmethod
    async def fetch_user_by_username(username: str) -> UserModel:

        if (user := await UserModel.get_or_none(username=username)) is None:

            raise NotFound(category="user_404")

        return user

    @staticmethod
    async def fetch_user_by_id(user_id: int) -> UserModel:

        if (user := await UserModel.get_or_none(id=user_id)) is None:

            raise NotFound(category="user_404")

        return user


class UpdateUser:
    @classmethod
    async def set_default_username(cls, user: UserDB):
        if not (user_obj := await UserModel.filter(id=user.id).first()):
            raise NotFound(category=USER_404)

        default_username = "user_" + str(user.id)[-8:]
        user_obj.username = default_username
        await user_obj.save(update_fields=("username",))

    @classmethod
    async def update_user(
        cls, user_id: int, form: UserUpdateForm, profile_img_path: str = None,
    ):

        user_obj = await UserModel.filter(id=user_id).first()

        update_data = form.__dict__
        update_data_filtered = {k: v for k, v in update_data.items() if v}
        if profile_img_path:
            update_data_filtered["profile_img_path"] = profile_img_path

        await user_obj.update_from_dict(update_data_filtered).save()

        return user_obj
