from typing import List, Union

from tortoise.queryset import QuerySet

from app.api.errors.error_categories import (
    INBOX_REQUEST_404,
    INVALID_INBOX_STATUS_UPDATE,
    INVALID_OUTBOX_STATUS_UPDATE,
    OUTBOX_REQUEST_404,
)
from app.api.errors.forbidden_handler import Forbidden
from app.api.errors.not_found_error import NotFound
from app.core.log.current_logger import CurrentLogger
from app.core.security.permissions import (
    RequestIssuerPermissions,
    RequestReceiverPermissions,
)
from app.db.models import RequestModel, UserModel
from app.schemes.request import (
    CreateRequestModel,
    Inbox_Pydantic,
    Outbox_Pydantic,
    UpdateRequestIssuerSide,
    UpdateRequestReceiverSide,
)
from app.schemes.user import UserDB


class CreateRequest:
    @staticmethod
    async def create_new_request(
        data: CreateRequestModel,
        send_by: UserDB,
    ):
        return await RequestModel.create(
            requested_by_id=send_by.id, **data.dict(exclude_unset=True)
        )


class RetrieveRequest:
    @classmethod
    async def fetch_inbox(cls, user_id: int):

        return await Inbox_Pydantic.from_queryset(
            RequestModel.filter(requested_item__owner__id=user_id).all(),
        )

    @classmethod
    async def fetch_request(cls, request_id: int):
        return RequestModel.filter(id=request_id).all()

    @classmethod
    async def fetch_inbox_request(
        cls, user_id: int, inbox_id: int,
    ) -> RequestModel:

        request_queryset = await cls.fetch_request(request_id=inbox_id)

        if not (
            inbox_request := await request_queryset.filter(
                requested_item__owner__id=user_id,
            ).first()
        ):
            raise NotFound(category=INBOX_REQUEST_404)

        return inbox_request

    @classmethod
    async def fetch_outbox(cls, user_id: int) -> List[Outbox_Pydantic]:

        user = await UserModel.filter(id=user_id).first()
        return await Outbox_Pydantic.from_queryset(user.outbox.all())

    @classmethod
    async def fetch_outbox_request(
        cls, user_id: int, outbox_id: int,
    ) -> RequestModel:

        request_queryset = await cls.fetch_request(request_id=outbox_id)

        if not (
            outbox_request := await request_queryset.filter(
                requested_by_id=user_id,
            ).first()
        ):

            raise NotFound(category=OUTBOX_REQUEST_404)

        return outbox_request

    @classmethod
    async def fetch_requests_per_item(
        cls, item_id: int,
    ) -> QuerySet[RequestModel]:
        return RequestModel.filter(requested_item_id=item_id).all()


class UpdateRequest:
    @classmethod
    async def update_request(
        cls,
        request_id: int,
        user_id: int,
        data: Union[UpdateRequestReceiverSide, UpdateRequestIssuerSide],
    ) -> RequestModel:

        if isinstance(data, UpdateRequestReceiverSide):

            request = await cls._update_inbox_checkpoint(
                user_id=user_id, request_id=request_id, data=data,
            )

        else:

            request = await cls._update_outbox_checkpoint(
                user_id=user_id, request_id=request_id, data=data,
            )

        await request.update_from_dict(data.dict()).save()

        return request

    @classmethod
    async def _update_inbox_checkpoint(
        cls, user_id: int, request_id: int, data: UpdateRequestReceiverSide,
    ) -> RequestModel:

        request = await RetrieveRequest.fetch_inbox_request(
            user_id=user_id, inbox_id=request_id,
        )
        if not (
            await RequestReceiverPermissions.is_updatable(
                current_status=request.status,
                update_value=data.dict().get("status"),
            )
        ):
            raise Forbidden(category=INVALID_INBOX_STATUS_UPDATE)

        return request

    @classmethod
    async def _update_outbox_checkpoint(
        cls, user_id: int, request_id: int, data: UpdateRequestIssuerSide,
    ) -> RequestModel:

        request = await RetrieveRequest.fetch_outbox_request(
            user_id=user_id, outbox_id=request_id,
        )
        if not (
            await RequestIssuerPermissions.is_updatable(
                current_status=request.status,
                update_value=data.dict().get("status"),
            )
        ):
            raise Forbidden(category=INVALID_OUTBOX_STATUS_UPDATE)

        return request
