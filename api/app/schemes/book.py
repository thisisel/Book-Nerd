from typing import Optional

from pydantic.main import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from ..db.models import BookModel

prefetch_book_exclude_fields = (
    "book.author",
    "book.isbn",
    "book.translated_by",
    "book.genre",
    "book.language",
    "book.cover_img",
    "book.cover_description",
)

BookInShelf_Pydantic = pydantic_model_creator(
    BookModel,
    name="BookInShelf",
    exclude=(
        "translated_by",
        "author",
        "genre",
        "cover_description",
        "language",
        "cover_img",
        "page_number",
        "copies",
    ),
    # exclude_readonly=True
    # include=(
    #     "isbn"
    # )
)
BookInPublicShelf_Pydantic = pydantic_model_creator(
    BookModel,
    name="BookInPublicShelf",
    exclude=("requests", "registered_on", "verified", "in_public_profile"),
)


class BookGistData(BaseModel):
    id: int
    isbn: str
    title: str
    author: str
    translated_by: Optional[str]
    published_by: str
