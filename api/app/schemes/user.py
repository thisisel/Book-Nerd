from datetime import datetime
from typing import Optional, Union

from fastapi import File, Form, UploadFile
from fastapi_users import models
from pydantic import UUID4, EmailStr, validator
from pydantic.main import BaseModel
from tortoise.contrib.pydantic import PydanticModel
from tortoise.contrib.pydantic.creator import pydantic_model_creator

from app.db.models import UserModel

from .common_models import ApiBaseResponse


#  inherited  pydantic base
#   defines optional basic fields and validation
# originally inherited a method that turns the instance into dict, excluding
#  everything that is unset
class User(models.BaseUser):
    username: Optional[str]
    joined_date: Optional[datetime] = None

    # @validator("username", always=True)
    # def defult_username(cls, v):
    #     return "user"+str(super().id)[0:4] if super().id else v

    @validator("joined_date", always=True)
    def defult_joindate(cls, v):
        return v or datetime.now()


#   inherited pydantic model for creation
#  compulsory user registration email and password compulsory
class UserCreate(models.BaseUserCreate):
    # username: Optional[str]
    pass


#  inherited pydantic model for updating
#  user profile update, password is optional
class UserUpdate(User, models.BaseUserUpdate):
    username: Optional[str]
    email: Optional[EmailStr]


# Pydantic model of a DB representation of a user,
# all attributes of Base User are now concrete
# added hashed password
class UserDB(User, models.BaseUserDB, PydanticModel):
    username: Optional[str]
    firstname: Optional[str]
    lastname: Optional[str]
    phonenumber: Optional[str]
    profile_img_path: Optional[str]
    ostan_id: Optional[int]
    shahr_id: Optional[int]

    class Config:
        orm_mode = True
        orig_model = UserModel  # for PydanticModel of tortoise


class PrivateProfileResponse(ApiBaseResponse):
    profile: User


class UserBaseNestedScheme(BaseModel):
    id: UUID4
    email: EmailStr
    username: Optional[str]


User_Pydantic = pydantic_model_creator(UserModel)
UserInbox_Pydantic = pydantic_model_creator(
    UserModel,
    name="User Inbox",
    exclude=("shelf", "sendbox", "joined_date", "email", "username"),
)
