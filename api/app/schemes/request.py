from datetime import datetime
from typing import Optional, Union

from pydantic import BaseModel
from pydantic.fields import Field
from pydantic.types import UUID4
from tortoise.contrib.pydantic.creator import pydantic_model_creator

from app.db.models import RequestModel
from app.schemes.common_models import ApiBaseResponse, CreateUpdateDictModel

Request_Pydantic = pydantic_model_creator(RequestModel)
Inbox_Pydantic = pydantic_model_creator(
    RequestModel,
    exclude=(
        "note",
        "requested_item.verified",
        "requested_item.in_public_profile",
        "requested_item.is_available",
        "requested_item.borrow_interval",
        "requested_item.date_added",
        "requested_item.date_modified",
        "requested_item.owner_id" "requested_item.book",
    ),
)
Outbox_Pydantic = pydantic_model_creator(
    RequestModel,
    exclude=(
        "note",
        "requested_by",
        "requested_by_id",
        "requested_item.verified",
        "requested_item.in_public_profile",
        "requested_item.is_available",
        "requested_item.borrow_interval",
        "requested_item.date_added",
        "requested_item.date_modified",
        "requested_item.owner",
        "requested_item.book",
    ),
)
# Request_Pydantic = pydantic_model_creator(
#     RequestModel,
#     exclude=(
#         "note",
#         "requested_by",
#         "requested_by_id",
#         "requested_item",
#         "requested_item_id",
#     )
# )


class BorrowRequestBody(BaseModel):
    # sender_id: UUID4
    note: Optional[str]


class NewBorrowRequest(BaseModel):
    id: int
    status: str = 2
    submitted: datetime
    last_modified: datetime
    note: str
    requested_item_id: str


class NewBorrowRequestResponse(ApiBaseResponse):
    request_data: NewBorrowRequest


class BookRequestApiResponse(ApiBaseResponse):
    data: Union[Outbox_Pydantic, Inbox_Pydantic]


class CreateRequestDictModel(CreateUpdateDictModel):
    def create_dict(self):
        return self.dict(
            exclude_unset=True,
            exclude={
                "id",
                "submitted",
                "last_modified",
            },
        )


class BaseRequest(CreateRequestDictModel):
    """Base Request model"""

    id: Optional[int] = None
    status: Optional[int] = 2
    submitted: Optional[datetime] = datetime.utcnow()
    last_modified: Optional[datetime] = datetime.utcnow()
    note: Optional[str] = None
    requested_by_id: Optional[UUID4] = None
    requested_item_id: Optional[int] = None


class CreateRequestModel(CreateRequestDictModel):
    requested_item_id: int
    note: Optional[str]


class UpdateRequestReceiverSide(BaseModel):
    status: int = Field()


class UpdateRequestIssuerSide(BaseModel):
    status: Optional[int]
    note: Optional[str]
