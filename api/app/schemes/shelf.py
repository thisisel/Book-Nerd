from datetime import datetime
from typing import List, Optional

from pydantic.main import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.schemes.common_models import ApiBaseResponse
from app.schemes.request import Inbox_Pydantic, Request_Pydantic

from ..db.models import LibraryModel
from .book import (
    BookGistData,
    BookInShelf_Pydantic,
    prefetch_book_exclude_fields,
)
from .user import UserBaseNestedScheme

ShelfItemInProfile_Pydantic = pydantic_model_creator(
    LibraryModel,
    name="ShelfItemInProfile",
    exclude=("book", "book_id", "owner", "owner_id"),
)

ShelfItemPubic_Pydantic = pydantic_model_creator(
    LibraryModel,
    name="ShelfItemForPublic",
    exclude=(
        "verified",
        "owner.email",
        "in_public_profile",
        *prefetch_book_exclude_fields,
    ),
)
CreateShelfItem_Pydantic = pydantic_model_creator(
    LibraryModel,
    name="AddNewShelfItem",
    exclude=(
        "verified",
        "owner_id",
    ),
    exclude_readonly=True,
)


class ShelfItemDataPublic(BaseModel):
    id: int
    borrow_interval: int
    is_available: bool
    date_added: datetime
    date_modified: datetime


class ShelfItemPublicResponse(ApiBaseResponse):
    item: ShelfItemPubic_Pydantic


class BookItemBundle(BaseModel):
    item_data: ShelfItemDataPublic
    book_data: BookGistData


class PublicShelfItemsResponse(ApiBaseResponse):
    owner_data: UserBaseNestedScheme
    shelf_items: List[BookItemBundle]


class PrivateShelfItemResponse(ApiBaseResponse):
    item: ShelfItemInProfile_Pydantic
    book: BookInShelf_Pydantic
    requests: Optional[List[Request_Pydantic]] = None
