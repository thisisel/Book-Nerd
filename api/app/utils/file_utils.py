import os
import secrets
from fastapi import UploadFile
from tempfile import SpooledTemporaryFile
from PIL import Image

def get_random_filename(file: UploadFile)-> str:
    
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(file.filename)
    filename_rnd = random_hex + f_ext

    return filename_rnd


def get_picture_path(file: UploadFile, root_path: str)-> str:

    filename = get_random_filename(file=file)
    picture_path = os.path.join(root_path, "static/profile", filename)

    return picture_path

def manipulate_save_thumbnail(file: SpooledTemporaryFile, picture_path: str):
    output_size = (125, 125)
    i = Image.open(file)
    i.thumbnail(output_size)

    i.save(picture_path)