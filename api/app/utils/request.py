from fastapi.encoders import jsonable_encoder

from app.core.log.current_logger import get_logger
from app.db.models import RequestModel
from app.schemes.request import NewBorrowRequest, NewBorrowRequestResponse


class Requests:
    @staticmethod
    async def compose_new_request_response(
        new_request: RequestModel,
    ) -> NewBorrowRequestResponse:

        request = jsonable_encoder(new_request)
        get_logger().debug(request)

        return NewBorrowRequestResponse(
            status=True,
            message="Request composed successfully",
            request_data=NewBorrowRequest(**request),
        )
