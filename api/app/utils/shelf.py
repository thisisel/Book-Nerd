from typing import List

from fastapi.encoders import jsonable_encoder

from app.db.models import LibraryModel
from app.schemes.shelf import (
    BookGistData,
    BookItemBundle,
    PublicShelfItemsResponse,
    ShelfItemDataPublic,
)
from app.schemes.user import UserBaseNestedScheme


class ShelfItemUtils:
    @classmethod
    async def compose_public_item_response(cls, owner, item, book):
        from app.schemes.shelf import (
            BookGistData,
            ShelfItemDataPublic,
            ShelfItemPublicResponse,
            UserBaseNestedScheme,
        )

        return ShelfItemPublicResponse(
            status=True,
            message="Item successfully retrived",
            owner_data=UserBaseNestedScheme(**owner),
            book_data=BookGistData(**book),
            item_data=ShelfItemDataPublic(**item),
        )

    @classmethod
    async def compose_public_shelf_items_response(
        cls, owner: dict, shelf: List[LibraryModel],
    ):

        return PublicShelfItemsResponse(
            status=True,
            message="Shelf items successfully retrived",
            owner_data=UserBaseNestedScheme(**owner),
            shelf_items=await cls.compose_item_book(shelf=shelf),
        )

    @classmethod
    async def compose_item_book(
        cls, shelf: List[LibraryModel],
    ) -> List[BookItemBundle]:

        shelf_items: List[BookItemBundle] = []

        for item in shelf:
            await item.fetch_related("book")
            book = item.book

            item_j = jsonable_encoder(item)
            item_data = ShelfItemDataPublic(**item_j)

            book_j = jsonable_encoder(book)
            book_data = BookGistData(**book_j)

            bundle = BookItemBundle(item_data=item_data, book_data=book_data)

            shelf_items.append(bundle)

        return shelf_items
